package com.mcsprogramacion.aplicaciontienda.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mcsprogramacion.aplicaciontienda.R;
import com.mcsprogramacion.aplicaciontienda.models.Item;

import java.util.List;

public class ItemListArrayAdapter extends ArrayAdapter {

    private final Context context;

    private final List<Item> itemList;

    /**
     * Constructor que permite cargar las propiedades de esta clase, y que llama inicialmente
     * al constructor original de ArrayAdapter indicando el nombre del layout (item_row_layout)
     *
     * @param context    Contexto (p.e. getActivity())
     * @param itemList Lista de objetos que va a contener la lista
     */
    public ItemListArrayAdapter(Context context, List itemList) {
        super(context, R.layout.item_row_adapter, itemList);
        this.context = context;
        this.itemList = itemList;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Asociar este ArrayAdpater con el layout 'item_row_adapter' que se ha creado
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_row_adapter, parent, false);

        //Obtener el objeto Item correspondiente a la posición de la lista que ha seleccionado el usuario
        Item item = itemList.get(position);

        TextView textViewCategory = (TextView) rowView.findViewById(R.id.textViewCategory);
        textViewCategory.setText(item.getCategory().getName()+" "+item.getBrand());

        TextView textViewModel = (TextView) rowView.findViewById(R.id.textViewModel);
        textViewModel.setText(item.getModel());

        TextView textViewPrice = (TextView) rowView.findViewById(R.id.textViewPrice);
        textViewPrice.setText((item.getPrice().toString()));

        //Cargar la imágen correspondiente
        ImageView imageViewPhoto = (ImageView) rowView.findViewById(R.id.imageViewFavorite);
        if(item.isFavorite()) {
            //Log.d(getClass().getName(), "Dentro del if");
            int id = context.getResources().getIdentifier("drawable/favorite", "drawable", context.getPackageName());
            imageViewPhoto.setImageDrawable(context.getResources().getDrawable(id));
        } else {
            int id = context.getResources().getIdentifier("drawable/nofavorite", "drawable", context.getPackageName());
            imageViewPhoto.setImageDrawable(context.getResources().getDrawable(id));
        }

        return rowView;
    }
}