package com.mcsprogramacion.aplicaciontienda;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mcsprogramacion.aplicaciontienda.models.Item;
import com.mcsprogramacion.aplicaciontienda.models.ItemList;

import java.util.List;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment3 extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    /**
     * The dummy content this fragment is presenting.
     */
    //private DummyContent.DummyItem mItem;
    private Item item;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment3() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            //mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
            String idItemSelected = getArguments().getString(ARG_ITEM_ID);
            item = ItemList.ITEM_MAP.get(idItemSelected);
    }}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_detail3, container, false);

        Activity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(item.getModel());
            //item.getCategory().getName()+"\n"+item.getModel()

        }
        // Show the dummy content as text in a TextView.
        //if (mItem != null) {
        //    ((TextView) rootView.findViewById(R.id.item_detail)).setText(mItem.details);
        //}

        // Show the item content
        if(item != null) {
            //Obtener una referencia al botón indicando su ID (button1)
            Button buttonWeb = ((Button) rootView.findViewById(R.id.buttonWeb));
            //Asignar un objeto OnClickListener a la propiedad OnClickListener
            buttonWeb.setOnClickListener(new View.OnClickListener() {
                //Implementar el método onClick para este objeto OnClickListener
                public void onClick(View v) {
                        Uri paginaWeb;
                        switch(item.getBrand()){
                            case "Balay":
                                paginaWeb= Uri.parse("http://www.balay.es");
                                break;
                            case "Bosch":
                                paginaWeb= Uri.parse("http://www.wolterskluwer.es/nuestras-marcas/bosch");
                                break;
                            default:
                                paginaWeb= Uri.parse("http://www.google.es");
                        }

                        Intent webIntent = new Intent(Intent.ACTION_VIEW, paginaWeb);
                        startActivity(Intent.createChooser(webIntent,"Abrir la página con: "));
                }
            });
            //((TextView) rootView.findViewById(R.id.textViewCategory)).setText(item.getCategory().getName()+" "+item.getBrand());


            //((TextView) rootView.findViewById(R.id.textViewModel)).setText(item.getModel());
            //((TextView) rootView.findViewById(R.id.textViewDescription)).setText(item.getDescription());
            //((TextView) rootView.findViewById(R.id.textViewPrice)).setText(""+item.getPrice());

            //ImageView imageViewPhoto = (ImageView) rootView.findViewById(R.id.imageViewPhoto);
            //int id = getActivity().getResources().getIdentifier("drawable/"+item.getPhotoFileName(), "drawable", getActivity().getPackageName());
            //imageViewPhoto.setImageDrawable(getActivity().getResources().getDrawable(id));

        }


        return rootView;
    }
}
