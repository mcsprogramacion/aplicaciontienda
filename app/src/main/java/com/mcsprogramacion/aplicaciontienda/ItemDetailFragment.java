package com.mcsprogramacion.aplicaciontienda;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mcsprogramacion.aplicaciontienda.dummy.DummyContent;
import com.mcsprogramacion.aplicaciontienda.models.Item;
import com.mcsprogramacion.aplicaciontienda.models.ItemList;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    /**
     * The dummy content this fragment is presenting.
     */
    //private DummyContent.DummyItem mItem;
    private Item item;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            //mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
            String idItemSelected = getArguments().getString(ARG_ITEM_ID);
            item = ItemList.ITEM_MAP.get(idItemSelected);
    }}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_detail, container, false);

        Activity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(item.getModel());
            //item.getCategory().getName()+"\n"+item.getModel()

        }
        // Show the dummy content as text in a TextView.
        //if (mItem != null) {
        //    ((TextView) rootView.findViewById(R.id.item_detail)).setText(mItem.details);
        //}

        // Show the item content
        if(item != null) {
            ((TextView) rootView.findViewById(R.id.textViewCategory)).setText(item.getCategory().getName()+" "+item.getBrand());
            ((TextView) rootView.findViewById(R.id.textViewModel)).setText(item.getModel());
            ((TextView) rootView.findViewById(R.id.textViewDescription)).setText(item.getDescription());
            ((TextView) rootView.findViewById(R.id.textViewPrice)).setText(""+item.getPrice());

            //ImageView imageViewPhoto = (ImageView) rootView.findViewById(R.id.imageViewPhoto);
            //int id = getActivity().getResources().getIdentifier("drawable/"+item.getPhotoFileName(), "drawable", getActivity().getPackageName());
            //imageViewPhoto.setImageDrawable(getActivity().getResources().getDrawable(id));

        }


        return rootView;
    }
}
