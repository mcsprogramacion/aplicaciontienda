package com.mcsprogramacion.aplicaciontienda.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 2DAW04 on 15/10/2015.
 */
public class ItemList {
    public static List<Item> itemList = new ArrayList();
    public static Map<String, Item> ITEM_MAP = new HashMap<String, Item>();

    public static void loadSimpleSamples() {
        Item item;

        item= new Item();
        item.setId(1);
        item.setCategory(CategoryList.getCategoryById(1));
        item.setBrand("Balay");
        item.setModel("XVM2000");
        item.setDescription("8KG, 1200RPM");
        item.setStock(5);
        item.setPrice(499.99);
        item.setSupplier("GrupoSur");
        item.setFavorite(true);
        item.setPhotoFileName("lavadora");
        itemList.add(item);
        ITEM_MAP.put(String.valueOf(item.getId()), item);

        item= new Item();
        item.setId(2);
        item.setCategory(CategoryList.getCategoryById(2));
        item.setBrand("Balay");
        item.setModel("CNS1234");
        item.setDescription("No Frost, Inox");
        item.setStock(3);
        item.setPrice(700.50);
        item.setSupplier("GrupoSur");
        item.setFavorite(false);
        item.setPhotoFileName("frigorifico");
        itemList.add(item);
        ITEM_MAP.put(String.valueOf(item.getId()), item);

        item= new Item();
        item.setId(3);
        item.setCategory(CategoryList.getCategoryById(3));
        item.setBrand("Bosch");
        item.setModel("RTV3989");
        item.setDescription("8KG, 1200RPM");
        item.setStock(4);
        item.setPrice(600.20);
        item.setSupplier("GrupoSur");
        item.setFavorite(false);
        item.setPhotoFileName("lavavajillas");
        itemList.add(item);
        ITEM_MAP.put(String.valueOf(item.getId()), item);

        item= new Item();
        item.setId(4);
        item.setCategory(CategoryList.getCategoryById(4));
        item.setBrand("Bosch");
        item.setModel("JKM1234");
        item.setDescription("Blanco, Pirólisis");
        item.setStock(1);
        item.setPrice(460.55);
        item.setSupplier("GrupoSur");
        item.setFavorite(true);
        item.setPhotoFileName("horno");
        itemList.add(item);
        ITEM_MAP.put(String.valueOf(item.getId()), item);
    }
}
