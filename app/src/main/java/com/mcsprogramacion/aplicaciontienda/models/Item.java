package com.mcsprogramacion.aplicaciontienda.models;

/**
 * Created by 2DAW04 on 15/10/2015.
 */
public class Item {
    private int id;
    private Category category;
    private String brand;
    private String model;
    private String description;
    private int stock;
    private Double price;
    private String supplier;
    private boolean favorite;
    private String photoFileName;


    public Item(){
    }

    public Item(boolean favorite,String photoFileName, int id, Category category, String brand, String model, String description, int stock, Double price, String supplier) {
        this.id = id;
        this.category = category;
        this.brand = brand;
        this.model = model;
        this.description = description;
        this.stock = stock;
        this.price = price;
        this.supplier = supplier;
        this.favorite = favorite;
        this.photoFileName = photoFileName;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getPhotoFileName() {
        return photoFileName;
    }

    public void setPhotoFileName(String photoFileName) {
        this.photoFileName = photoFileName;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", category=" + category +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", description='" + description + '\'' +
                ", stock=" + stock +
                ", price=" + price +
                ", supplier='" + supplier + '\'' +
                ", favorite=" + favorite +
                ", photoFileName='" + photoFileName + '\'' +
                '}';
    }
}

