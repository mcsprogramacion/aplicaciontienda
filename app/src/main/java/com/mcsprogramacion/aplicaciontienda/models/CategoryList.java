package com.mcsprogramacion.aplicaciontienda.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2DAW04 on 16/10/2015.
 */
public class CategoryList {
    public static List<Category> categoryList = new ArrayList();

    public static Category getCategoryById(int id){
        for(Category c : categoryList) {
            if(c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    public static Category getCategoryByname(String name){
        for(Category c : categoryList) {
            if(c.getName() == name) {
                return c;
            }
        }
        return null;
    }

    public static void loadSimpleSamples() {
        Category category;

        category = new Category();
        category.setId(1);
        category.setName("Lavadora");
        categoryList.add(category);

        category = new Category();
        category.setId(2);
        category.setName("Frigorifico");
        categoryList.add(category);

        category = new Category();
        category.setId(3);
        category.setName("Lavavajillas");
        categoryList.add(category);

        category = new Category();
        category.setId(4);
        category.setName("Horno");
        categoryList.add(category);
    }
}
